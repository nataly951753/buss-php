<?php 

function read_data($filename, &$station_count, &$station,&$direction,&$persons)
{
	if (($f = fopen($filename, 'r')) !== false ) {

        $data=fread($f, filesize($filename));
	$mas_data=json_decode($data,true);
	$station_count=$mas_data['station_count'];
	$station=$mas_data['station'];
        $direction=$mas_data['direction'];
        $persons=$mas_data['persons'];

	fclose($f);
	}
	else{
	$station_count=rand(10,20);
	$station=1;
        $direction=true;
        $persons=[];
	}
}
function write_data($filename, $station_count, $station,$direction,$persons){
if (($f = fopen($filename, 'w')) !== false) {
        $data_to_write=[
                'station_count'=>$station_count,
                'direction'=>$direction,
                'persons'=>$persons,
                'station'=>$station
        ];       
        fwrite($f, json_encode($data_to_write));
        fclose($f);
        }
}
function read_data_temp($filename,&$persons)
{
        if (($f = fopen($filename, 'r')) !== false ) {
        $data=fread($f, filesize($filename));
        $mas_data=json_decode($data,true);
        $persons=$mas_data['persons'];
                fclose($f);
        }
        else{
        $persons=[];
        }
}
function write_data_temp($filename,$persons){
if (($f = fopen($filename, 'w')) !== false) {
        $data_to_write=[
                'persons'=>$persons
        ];       
        fwrite($f, json_encode($data_to_write));
        fclose($f);
        }
}
 ?>

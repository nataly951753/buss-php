<?php 
error_reporting(E_ERROR );
require('data.php');
require('work_with_file.php');
require('work_with_persons.php');
require('work_with_cookie.php');
require('show_data.php');

$filename='users_data/' . $_COOKIE['id'] . '.txt';
$file_name_temp='temp.txt';

if(isset($_POST['LogOut'])){    
    //получаем начальные данные
    read_data_temp($file_name_temp, $persons);
    //сохраняем информацию
    write_data($filename, $_COOKIE['station_count'], $_COOKIE['station'],$_COOKIE['direction'],$persons);  
    //очищаем куки
    delete_cookie();

}


if(isset($_COOKIE['id']))
{
    $station_count=$_COOKIE['station_count'];
    $direction=$_COOKIE['direction'];
    $station=$_COOKIE['station'];
    $person_in=[];
    $person_out=[];
    $person_in_count=rand(0,20);//вошедшие


if(isset($_POST['run']))
{
    //получаем начальные данные из temp файла
    read_data_temp($file_name_temp, $persons);
}
else
{
     //получаем начальные данные из users_data
    read_data_temp($filename, $persons);
}

//удаляем тех, кто выходит
persons_arrival($persons,$station,$person_out);


//рандомим тех, кто заходит 
persons_departure($persons,$person_in,$station,$station_count,$person_in_count,$name);


//следующая остановка
change_direction_and_station($direction,$station,$station_count);
//устанавливаем cookie
set_cookie_temp($station_count,$direction,$station);

//сохраняем информацию
write_data_temp($file_name_temp,$persons);
//возвращаемся к текущей остановкe для html
($direction) ? $station-- : $station++;
}
else{
    header('Location: /');
}



 ?>

 <!DOCTYPE html>
 <html>
 <head>
 	<title>Buss</title>
        <link rel="stylesheet" type="text/css" href="start.css">
 </head>
 <body>
 	<h5>Кол-во станций в маршруте: <?=$station_count ?></h5>
        <h5>Текущая станция: <?=$station ?></h5>
        <h5>Вошли: <?=$person_in_count ?></h5>
        <form action="" method="post" >
            <input type="hidden" name="LogOut" value="true">
            <input type="submit" value="Log Out"  class="out">
        </form>
        <?php print_stantion($station_count,$station); ?>
         <form action="" method="post" >
        <input type="hidden" name="run" value="run" >
        <input type="submit" value="Ехать!"  class="run">
        </form>
        <div class="table-desk">
        <table>
                <caption>Вышли</caption>
                <tr>
                        <th>ФИО</th>
                        <th>Зашёл на станции</th>
                        <th>Выйдет на станции</th>
                </tr>
                <?php print_person($person_out); ?>
        </table>

        <table>
                <caption>Вoшли</caption>
                <tr>
                        <th>ФИО</th>
                        <th>Зашёл на станции</th>
                        <th>Выйдет на станции</th>
                </tr>
                <?php print_person($person_in); ?>
        </table>
        <table>
                <caption>В автобусе</caption>
                <tr>
                        <th>ФИО</th>
                        <th>Зашёл на станции</th>
                        <th>Выйдет на станции</th>
                </tr>
                <?php print_person($persons); ?>

        </table>
</div>


 </body>
 </html>